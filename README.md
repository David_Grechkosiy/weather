Installation

virtualenv -p python3 weather
source bin/activate

pip install -r requirements/local.txt

Apply migrations: python manage.py migrate

Create superuser: python manage.py createsuperuser

run project: python manage.py runserver It should be accessable by url: http://localhost:8000/admin/