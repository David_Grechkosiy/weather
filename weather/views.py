import json
import requests

from datetime import datetime
from django.http import HttpResponse
from rest_framework import generics
from rest_framework.exceptions import ValidationError

from config.pagination import StandardResultsSetPagination
from config.settings import API_KEY

from .models import *
from .serializers import *


class WeatherListCreateView(generics.ListCreateAPIView):
    serializer_class = WeatherSerializer

    def get_queryset(self):
        return Weather.objects.all()

    def perform_create(self, serializer):
        city = self.request.data.get('select1')
        url = 'http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s' % (city, API_KEY)
        response = requests.get(url)
        data = response.json()
        if data['cod'] == '404':
            raise ValidationError({
                "city": [
                    'This city does not exists'
                ]
            })
        else:
            serializer.save(
                city=city.lower(),
                date=datetime.now().date(),
                temperature=data['main']['temp'] - 273,
                description=data['weather'][0]['description']
            )


def validate_date(value):
    try:
        datetime.strptime(value, '%Y-%m-%d')
    except ValueError:
        raise ValidationError({
                "date": [
                    'Incorrect data format, should be YYYY-MM-DD'
                ]
            })


class WeatherFilterListView(generics.ListAPIView):
    serializer_class = WeatherSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        query_params = self.request.query_params
        if 'filter1' in query_params:
            value = query_params.get('filter1')
            return Weather.objects.raw('SELECT * FROM weather_weather WHERE city like lower(%s)', [value])
        if 'filter2' in query_params:
            value = query_params.get('filter2')
            validate_date(value)
            return Weather.objects.raw('SELECT * FROM weather_weather WHERE date >= %s', [value])
        if 'filter3' in query_params:
            value = query_params.get('filter3')
            validate_date(value)
            return Weather.objects.raw('SELECT * FROM weather_weather WHERE date <= %s', [value])
