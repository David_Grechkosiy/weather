from django.db import models

class Weather(models.Model):
    city = models.CharField('City', max_length=50, null=True, blank=True)
    date = models.DateField('Date', null=True, blank=True)
    temperature = models.SmallIntegerField('Days left', null=True, blank=True)
    description = models.CharField('Description', max_length=50, null=True, blank=True)
